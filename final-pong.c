/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    int barvel = 5;
    int minvel = 3;
    int maxvel = 5;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    bool pause = false;
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    Rectangle player;
    player.width = 20;
    player.height = 100;    
    player.x = screenWidth - 50 - player.width;
    player.y = screenHeight/2 - player.height/2;
    int playerSpeedY = barvel;
    
    Rectangle enemy;
    enemy.width = 20;
    enemy.height = 100;    
    enemy.x = 50;
    enemy.y = screenHeight/2 - enemy.height/2;
    int enemySpeedY = barvel;
    
    int iaLinex = screenWidth/2;
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    
    Vector2 ballSpeed;
    ballSpeed.x = minvel;
    ballSpeed.y = minvel;
    
    int ballRadius = 20;
    
    int playerLife = 5;
    int enemyLife = 5;
        
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Lose, 1 - Win, -1 - Not defined
    
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    InitAudioDevice();
    
    Sound fxReb = LoadSound("Resources/Rebota.wav");
    Sound fxGol = LoadSound("Resources/Marca.wav");
    Music fxMusic = LoadMusicStream("Resources/effect.mp3");
    SetMusicVolume(fxMusic, 0.08f);
    SetSoundVolume(fxReb, 0.5f);
    SetSoundVolume(fxGol, 0.5f);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                framesCounter++;
                
                if (framesCounter < 90)
                {
                    alpha += fadeSpeed;
            
                    if(alpha >= 1.5f)
                    {
                        alpha = 1.5f;
                        fadeOut = !fadeOut;
                    }
                }
                if(framesCounter < 210)
                {
                    alpha -= fadeSpeed;
                    if(alpha <= 0.0f)
                    {
                        alpha = 0.0f;
                        fadeOut = !fadeOut;
                    }
                }
                if(framesCounter > 300)
                {
                    screen = TITLE;
                }
                
        // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                if (IsKeyPressed(KEY_ENTER)) screen = GAMEPLAY;
                
                framesCounter++;
                
                // TODO: Title animation logic.......................(0.5p)
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                
            } break;
            case GAMEPLAY:
            {
                // Update GAMEPLAY screen data here!
                UpdateMusicStream(fxMusic);
                
                if(!pause){
                    
                    if (IsKeyDown(KEY_Q)){
                    enemy.y -= barvel;
                    }
            
                    if (IsKeyDown(KEY_A)){
                    enemy.y += barvel;
                    }
                        
                    if (IsKeyDown(KEY_RIGHT)){
                        iaLinex+=barvel;
                    }
            
                    if (IsKeyDown(KEY_LEFT)){
                        iaLinex -=barvel;
                    }
                    
                    framesCounter++;
               
                    if (framesCounter % 60 == 0)
                    {
                        framesCounter = 0;
                        secondsCounter--;
                    }
               
                    if (secondsCounter < 0) screen = ENDING;
                }
             
                if( ballPosition.x > iaLinex){
                    if(ballPosition.y > player.y){
                        player.y+=barvel;
                    }
            
                    if(ballPosition.y < player.y){
                        player.y-=barvel;
                    }
                }
        
                if (IsKeyPressed(KEY_P)){
                pause = !pause;
                if(pause) PauseMusicStream(fxMusic);
                else ResumeMusicStream(fxMusic);
                }
                
                if(enemy.y<0){
                    enemy.y = 0;
                }
       
                if(enemy.y > (screenHeight - enemy.height)){
                    enemy.y = screenHeight - enemy.height;
                }
       
                if(player.y<0){
                    player.y = 0;
                }
       
                if(player.y > (screenHeight - player.height)){
                    player.y = screenHeight - player.height;
                }
       
                if(!pause){
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                }
       
                if(ballPosition.x > screenWidth - ballRadius){
                    enemyLife--;
                    PlaySound(fxGol);
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    ballSpeed.x = -minvel;
                    ballSpeed.y = minvel;
           
                }else if(ballPosition.x < ballRadius){
                    playerLife--;
                    PlaySound(fxGol);
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    ballSpeed.x = minvel;
                    ballSpeed.y = minvel;
                }
       
                if((ballPosition.y > screenHeight - ballRadius) || (ballPosition.y < ballRadius) ){
                    PlaySound(fxReb);
                    ballSpeed.y *=-1;
                }
 
 
                if(CheckCollisionCircleRec(ballPosition, ballRadius, player)){
                    if(ballSpeed.x>0){
                        if(abs(ballSpeed.x)<maxvel){                    
                            ballSpeed.x *=-1.3;
                            ballSpeed.y *= 1.3;
                        }else{
                            ballSpeed.x *=-1;
                        }
                    }
                }
       
                if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy)){
                    if(ballSpeed.x<0){
                        if(abs(ballSpeed.x)<maxvel){                    
                            ballSpeed.x *=-1.3;
                            ballSpeed.y *= 1.3;
                        }else{
                            ballSpeed.x *=-1;
                        }
                    }
                }
                
                if (playerLife <= 0) {
                    StopMusicStream(fxMusic);
                    gameResult = 1;
                    screen = ENDING;
                }
                
                if (enemyLife <= 0){
                    StopMusicStream(fxMusic);
                    gameResult = 0;
                    screen = ENDING;
                }
                // TODO: Ball movement logic.........................(0.2p)
                
                // TODO: Player movement logic.......................(0.2p)
                
                // TODO: Enemy movement logic (IA)...................(1p)
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                framesCounter++;
                
                if (IsKeyPressed(KEY_ENTER)){
                    screen = GAMEPLAY;
                    ballPosition = (Vector2) {screenWidth/2, screenHeight/2};
                    ballSpeed.x = minvel;
                    ballSpeed.y = minvel;
                    player.y = screenHeight/2 - player.height/2;
                    enemy.y = screenHeight/2 - enemy.height/2;
                    secondsCounter = 99;
                    gameResult = -1;
                    pause = false; 
                    playerLife = 5;
                    enemyLife = 5;
                    framesCounter = 0;
                    PlayMusicStream(fxMusic);
                }

                if (IsKeyPressed (KEY_ESCAPE)) {
                    CloseWindow;
                }
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                                       
                    DrawText("THE ULTIMATE PONG" , screenWidth/2 - MeasureText("THE ULTIMATE PONG", 50)/2, screenHeight/2, 50, BLACK);            
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    DrawText ("Try to win... If you can", screenWidth/2 - MeasureText("Try to win... If you can", 50)/2, screenHeight/2 - 50, 50, BLACK);
                    
                    if ((framesCounter/30)%2) DrawText("Press ENTER to try", screenWidth/2 - MeasureText("Press ENTER to try", 30)/2, screenHeight/2 + 50, 30, BLACK);
                    
                    // TODO: Draw Title..............................(0.2p)
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangle(0, 0, screenWidth, screenHeight, BLACK);
                    
                    DrawRectangleRec (player, GREEN);
                    
                    DrawRectangleRec (enemy, GREEN);
                    
                    DrawCircleV(ballPosition, ballRadius, GREEN);
                    
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2, 0, 40, GREEN);
                    
                    DrawText(FormatText("%d", playerLife), 100, 0, 40, GREEN);
                    
                    DrawText(FormatText("%d", enemyLife), screenWidth - 100, 0, 40, GREEN);
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    // TODO: Draw time counter.......................(0.5p)
                    if(pause){
                        DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                        DrawText("Press p to resume", screenWidth/2 - MeasureText("Press p to resume", 40)/2 , screenHeight/2, 40, RED);
                    }
                    // TODO: Draw pause message when required........(0.5p)
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    if (gameResult == 1) {
                        DrawText ("Woopsy, you lose", screenWidth/2 - MeasureText("Woopsy, you lose", 50)/2, screenHeight/2 - 50, 50, BLACK);
                        DrawText ("Wanna try again?", screenWidth/2 - MeasureText("Wanna try again?", 50)/2, screenHeight/2 + 10, 50, BLACK);
                        if ((framesCounter/30)%2) DrawText("Press ENTER to retry", screenWidth/2 - MeasureText("Press ENTER to retry", 30)/2, screenHeight/2 + 75, 30, BLACK);
                    }
                    
                    else {
                        DrawText ("That's not even my final stage", screenWidth/2 - MeasureText("That's not even my final stage", 50)/2, screenHeight/2 - 50, 50, BLACK);
                        DrawText ("Wanna play again?", screenWidth/2 - MeasureText("Wanna play again?", 50)/2, screenHeight/2 + 10, 50, BLACK);
                        if ((framesCounter/30)%2) DrawText("Press ENTER to retry", screenWidth/2 - MeasureText("Press ENTER to retry", 30)/2, screenHeight/2 + 75, 30, BLACK);
                    }
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
            
            
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadSound(fxReb); 
    UnloadSound(fxGol);
    UnloadMusicStream(fxMusic);
 
    CloseAudioDevice(); 
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}